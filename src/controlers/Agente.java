package controlers;

import java.util.*;
import java.io.*;

public class Agente implements Comparable<Agente> {
    private String SplitBy = ",";
    private String nombre;
    private String cedula;
    private String imagePath;
    private Ubicacion ubicacion;

    public Agente(String inombre, String icedula, String iimagePath,Ubicacion iubicacion)
    {
        this.nombre     = inombre;
        this.cedula     = icedula;
        this.imagePath  = iimagePath;
        this.ubicacion  = iubicacion;
    };

    @Override
    public int compareTo(Agente o) {
        return this.nombre.compareTo(o.nombre);
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public String getCedula()
    {
        return this.cedula;
    }

    public String getImagePath()
    {
        return this.imagePath;
    }

    public Ubicacion getUbicacion()
    {
        return this.ubicacion;
    }

    public void setNombre(String inombre)
    {
        this.nombre = inombre;
    }

    public void setCedula(String icedula)
    {
        this.cedula = icedula;
    }

    public void setImagePath(String iimagePath)
    {
        this.imagePath  = iimagePath;
    }

    public void setUbicacion(Ubicacion iubicacion)
    {
        this.ubicacion = iubicacion;
    }

    public Set<Agente> cargarAgentesArchivo(String nombre_archivo)
    {
        String line;
        List<Agente> result = new ArrayList<Agente>();

        BufferedReader br;
        try {
            br = new BufferedReader( new InputStreamReader(new FileInputStream(nombre_archivo), "UTF8"));
            try
            {
                while ((line = br.readLine()) != null)
                {
                    String[] content = line.split(SplitBy);
                    String newNombre = content[0];
                    String newCedula = content[1];
                    String newImagePath = content[2];
                    String[] newUbicaciones = content[3].split(":");
                    double newUbicacionX = Double.parseDouble(newUbicaciones[0]);
                    double newUbicacionY = Double.parseDouble(newUbicaciones[1]);
                    Ubicacion newUbicacion = new Ubicacion(newUbicacionX,newUbicacionY);
                    Agente newAgente = new Agente(newNombre,newCedula,newImagePath,newUbicacion);
                    result.add(newAgente);
                }
            }
            catch (IOException ex)
            {

            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Collections.sort(result);
        Set<Agente> resultSet =  new HashSet<Agente>(result);
        return resultSet;
    }

    @Override
    public boolean equals(Object obj) {
        Agente otro = (Agente) obj;
        return ((this.nombre.equals(otro.nombre)) && (this.cedula.equals(otro.cedula) ));
    }

    @Override
    public int hashCode() {
        int prime = 31;
        int result = 1;
        result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
        result = prime * result + ((cedula == null) ? 0 : cedula.hashCode());
        return  result;
    }
}
