package controlers;

public class Ubicacion {
    private double ubicacionX;
    private double ubicacionY;

    public Ubicacion(double iubicacionX, double iubicacionY)
    {
        ubicacionX = iubicacionX;
        ubicacionY = iubicacionY;
    }

    public void setUbicacionX(double x)
    {
        this.ubicacionX = x;
    }

    public void setUbicacionY(double y)
    {
        this.ubicacionY = y;
    }

    public double getUbicacionX()
    {
        return this.ubicacionX;
    }

    public double getUbicacionY()
    {
        return this.ubicacionY;
    }

    public static double calcularDistancia(Ubicacion u1, Ubicacion u2)
    {
        double x1 = u1.getUbicacionX();
        double y1 = u1.getUbicacionY();
        double x2 = u2.getUbicacionX();
        double y2 = u2.getUbicacionY();
        double fact1 = Math.pow((x2 - x1),2);
        double fact2 = Math.pow((y2 - y1),2);
        double d = Math.pow((fact1 + fact2),(1/2));
        return d;
    }
}
