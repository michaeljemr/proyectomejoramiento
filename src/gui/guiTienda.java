package gui;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class guiTienda {

    public static Stage parent;


    public static void Display(Stage iparent)
    {
        parent = iparent;
        Stage window = new Stage();
        window.setResizable(false);
        window.setTitle("Tienda");
        window.setWidth(600);
        window.setHeight(380);

        Button button1;
        button1 = new Button();
        button1.setText("Regresar");
        button1.setLayoutX(50);
        button1.setLayoutY(30);
        button1.setOnAction(e ->
        {
            parent.show();
            window.close();
        });

        Pane layout = new Pane();
        layout.getChildren().add(button1);

        Scene scene = new Scene(layout,600,380);
        window.setScene(scene);
        window.show();
    }
}