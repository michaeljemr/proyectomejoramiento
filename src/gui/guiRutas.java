package gui;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.stage.Stage;

public class guiRutas {

    public static Stage parent;

    public guiRutas()
    {
    }
    public void Display(Stage iparent)
    {
        parent = iparent;
        Stage window = new Stage();
        window.setResizable(false);
        window.setTitle("Rutas");
        window.setWidth(1000);
        window.setHeight(500);

        Button button1;
        button1 = new Button();
        button1.setText("Regresar");
        button1.setLayoutX(830);
        button1.setLayoutY(30);
        button1.setOnAction(e ->
        {
            parent.show();
            window.close();
        });


        Canvas canvas = new Canvas(800, 450);
        canvas.setLayoutX(10);
        canvas.setLayoutY(10);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        drawShapes(gc);

        Pane layout = new Pane();
        layout.getChildren().add(button1);
        layout.getChildren().add(canvas);

        Scene scene = new Scene(layout,1000,500);
        window.setScene(scene);
        window.show();
    }

    private void drawShapes(GraphicsContext gc) {
        gc.setFill(Color.RED);        
        Image image = new Image(getClass().getResourceAsStream("/gui/mapa.jpg"));
        gc.drawImage(image,0, 0,800,450);
        gc.fillOval(100, 400, 20, 20);
       
    }
}