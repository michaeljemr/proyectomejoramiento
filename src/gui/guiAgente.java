package gui;

import javafx.scene.Scene;
import java.io.File;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;


public class guiAgente {

    public static Stage parent;


    public static void Display(Stage iparent)
    {
        parent = iparent;
        Stage window = new Stage();
        window.setResizable(false);
        window.setTitle("Agentes");
        window.setWidth(600);
        window.setHeight(380);

        Button button1;
        button1 = new Button();
        button1.setText("Aceptar");
        button1.setLayoutX(350);
        button1.setLayoutY(280);
        button1.setOnAction(e ->
        {
            parent.show();
            window.close();
        });

        Text txt_nombre = new Text();
        txt_nombre.setText("Ingrese Nombre de Agente");
        txt_nombre.setLayoutX(150);
        txt_nombre.setLayoutY(35);

        TextField txtf_nombre = new TextField();
        txtf_nombre.setMinWidth(200);
        txtf_nombre.setLayoutX(300);
        txtf_nombre.setLayoutY(20);


        Text txt_cedula = new Text();
        txt_cedula.setText("Ingrese Cedula de Agente");
        txt_cedula.setLayoutX(150);
        txt_cedula.setLayoutY(85);

        TextField txtf_cedula = new TextField();
        txtf_cedula.setMinWidth(200);
        txtf_cedula.setLayoutX(300);
        txtf_cedula.setLayoutY(70);

        Text txt_imagen = new Text();
        txt_imagen.setText("Imagen de Agente");
        txt_imagen.setLayoutX(150);
        txt_imagen.setLayoutY(135);

        final FileChooser fileChooser = new FileChooser();
        final Button openButton = new Button("Seleccionar");
        openButton.setOnAction(e -> {
            File file = fileChooser.showOpenDialog(window);
        });
        openButton.setLayoutX(300);
        openButton.setLayoutY(120);


        Text txt_ubicX = new Text();
        txt_ubicX.setText("Ingrese Ubicacion en X");
        txt_ubicX.setLayoutX(150);
        txt_ubicX.setLayoutY(185);

        TextField txtf_ubicX = new TextField();
        txtf_ubicX.setMinWidth(200);
        txtf_ubicX.setLayoutX(300);
        txtf_ubicX.setLayoutY(170);


        Text txt_ubicY = new Text();
        txt_ubicY.setText("Ingrese Ubicacion en Y");
        txt_ubicY.setLayoutX(150);
        txt_ubicY.setLayoutY(235);

        TextField txtf_ubicY = new TextField();
        txtf_ubicY.setMinWidth(200);
        txtf_ubicY.setLayoutX(300);
        txtf_ubicY.setLayoutY(220);


        Pane layout = new Pane();
        layout.getChildren().add(button1);
        layout.getChildren().add(txt_nombre);
        layout.getChildren().add(txtf_nombre);

        layout.getChildren().add(txt_cedula);
        layout.getChildren().add(txtf_cedula);

        layout.getChildren().add(txt_imagen);
        layout.getChildren().add(openButton);

        layout.getChildren().add(txt_ubicX);
        layout.getChildren().add(txtf_ubicX);

        layout.getChildren().add(txt_ubicY);
        layout.getChildren().add(txtf_ubicY);

        Scene scene = new Scene(layout,600,380);
        window.setScene(scene);
        window.show();
    }
}
