package sample;

import gui.guiAgente;
import gui.guiAsignacion;
import gui.guiRutas;
import gui.guiTienda;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Dev extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage)
    {
        Stage window;

        window = stage;
        window.setResizable(false);
        window.setTitle("Bienvenido al Sistema");
        window.setWidth(600);
        window.setHeight(380);

        Button button1;
        button1 = new Button();
        button1.setText("Creacion de Agentes");
        button1.setLayoutX(50);
        button1.setLayoutY(30);
        button1.setOnAction(e ->
        {
            window.hide();
            guiAgente.Display(window);
        });

        Button button2;
        button2 = new Button();
        button2.setText("Creacion de Tiendas");
        button2.setLayoutX(50);
        button2.setLayoutY(60);
        button2.setOnAction(e ->
        {
            window.hide();
            guiTienda.Display(window);
        });

        Button button3;
        button3 = new Button();
        button3.setText("Asignacion de Tiendas a Agentes");
        button3.setLayoutX(50);
        button3.setLayoutY(90);
        button3.setOnAction(e ->
        {
            window.hide();
            guiAsignacion.Display(window);
        });

        Button button4;
        button4 = new Button();
        button4.setText("Visualizador de Rutas");
        button4.setLayoutX(50);
        button4.setLayoutY(120);
        button4.setOnAction(e ->
        {
            window.hide();
            guiRutas guiRutas1 = new guiRutas();
            guiRutas1.Display(window);
        });

        Pane layout = new Pane();
        layout.getChildren().add(button1);
        layout.getChildren().add(button2);
        layout.getChildren().add(button3);
        layout.getChildren().add(button4);

        Scene scene = new Scene(layout,600,380);
        window.setScene(scene);
        window.show();
    }
}
